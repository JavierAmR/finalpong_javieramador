/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by Javier Amador
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"
#include "stdio.h"
#include "stdlib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING, OPTIONS, PLAYERSELECTION } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    Rectangle player = {30, screenHeight/2, 10, 30};
    int playerSpeedY = 3;
    
    Rectangle enemy = {screenWidth - 40, screenHeight/2, 10, 30};;
    int enemySpeedY = 3;
    
    //BALL
    Vector2 ballPosition = {screenWidth/2, screenHeight/2};
    Vector2 ballSpeed = {3,3};
    
    if (GetRandomValue (0,1) == 0)
    {
        ballSpeed.x *= -1;        
    }
    if (GetRandomValue (0,1) == 0)
    {
        ballSpeed.y *= -1;        
    }
    
   
    float ballRadius = 5;
    
    Rectangle playerlifeBack = {5, 10, screenWidth/2 - 40, 10};
    Rectangle playerlifeFill = {10, 12, screenWidth/2 - 50, 6};
    Rectangle enemylifeBack = {screenWidth/2 + 40, 10, screenWidth/2 - 45, 10};    
    Rectangle enemylifeFill= {screenWidth/2 + 45, 12, screenWidth/2 - 55, 6};
    
    //int playerLife = 100;
    //int enemyLife = 100;
    
    int playerLifePoint = 20;
    int enemyLifePoint = 20;
    
    int secondsCounter = 99;
    
    //LOGO 
    bool fadeOut = true;
    float fadespeed = 0.015f;
    float alphalogo = 0;
    
    //TITLE
    float alphatitle = 0;
    float fadespeedtitle = 0.02f;
    bool blink = false;
    
    //GAMEPLAY
    bool pause = false;
        
    
    int framesCounter;          // General pourpose frames counter
    
    //int gameResult = -1;        // 0 - Loose, 1 - Win, -1 - Not defined
    
    //Variables de control de Opciones: 0 - Disabled, 1 - Enabled
    
    int cheatMenu = 0;
    int showFrames = 0;
    int difficulty = 2;
    int twoplayerMode = 0;
    
    // IA ball detector
    int ballDetector = screenWidth/2;
    
    // Player settings
    int playerPower = 0;
    int enemyPower = 0;
    
    int playerTrigger = 0;
    int enemyTrigger = 0;
    
    int playerPowerTimer = 4;
    int enemyPowerTimer = 4;
    
    int playerBar = 0;
    int enemyBar = 0;
    
    int playertimeStop = 0;
    int enemytimeStop = 0;
    
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    InitAudioDevice();
    Sound battleMusic = LoadSound ("music/battleTheme.mp3");
    Sound mainTheme = LoadSound ("music/mainTheme.mp3");
    Sound uisound = LoadSound ("music/UIsound.wav");
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        SetSoundVolume(mainTheme, 0.5f);
        SetSoundVolume(battleMusic, 0.5f);
        // Update
        //----------------------------------------------------------------------------------
        
        switch(screen) 
        {
            case LOGO: 
            {
                
                // Update LOGO screen data here!
                if (fadeOut) 
                {
                    alphalogo += fadespeed;
                    
                    if (alphalogo > 1.0f)
                    {
                        alphalogo = 1.0f;
                        fadeOut = !fadeOut;
                    }
                }
                else if (alphalogo > 0.0f)
                {
                    alphalogo -= fadespeed;
                    
                    if (alphalogo < 0.0f)
                    {
                        alphalogo = 0.0f;                        
                    }
                }
                else 
                {
                    screen = TITLE;
                }
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                
            } break;
            case TITLE: 
            {
                if (!IsSoundPlaying(mainTheme))
                {
                    PlaySound(mainTheme);
                }
                if (IsSoundPlaying(battleMusic))
                {
                    StopSound(battleMusic);
                }
                // Update TITLE screen data here!
                if (alphatitle < 1.0f) 
                {
                    alphatitle += fadespeedtitle;
                    
                    if (alphatitle > 1.0f)
                    {
                        alphatitle = 1.0f;
                        fadeOut = !fadeOut;
                    }
                }
                
                framesCounter ++;
                
                if (framesCounter % 40 == 0) 
                {
                    framesCounter = 0;
                    blink = !blink;
                }
                
                if (IsKeyPressed(KEY_ENTER))
                {
                    PlaySound (uisound);
                    screen = PLAYERSELECTION;
                    switch(difficulty)
                    {
                        case 1: ballDetector = screenWidth/2 + screenWidth/4;
                        break;
                        case 2: ballDetector = screenWidth/2;
                        break;
                        case 3: ballDetector = screenWidth/4;
                        break;
                        case 4: ballDetector = 0;
                        break;
                    }
                }
                if (IsKeyPressed(KEY_O))
                {
                    PlaySound (uisound);
                    screen = OPTIONS;
                }
                
                
                // TODO: Title animation logic.......................(0.5p)
                
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                
            } break;
            case GAMEPLAY:
            { 
                if (IsSoundPlaying(mainTheme))
                {
                    StopSound (mainTheme);
                }
                if (!IsSoundPlaying(battleMusic))
                {
                    PlaySound (battleMusic);
                }
                
                if (playerTrigger == 1 || enemyTrigger == 1)
                {
                    SetSoundPitch (battleMusic, 0.7f);
                }
                if (playerTrigger == 0 && enemyTrigger == 0)
                {
                    SetSoundPitch (battleMusic, 1);
                }
                // Update GAMEPLAY screen data here!
                if (IsKeyPressed(KEY_P))
                {
                    PlaySound (uisound);
                    pause = !pause;
                }
                if (!pause)
                {
                // TODO: Ball movement logic.........................(0.2p)
                
                if (playertimeStop != 1 && enemytimeStop !=1 )
                {
                    ballPosition.x += ballSpeed.x;
                    ballPosition.y += ballSpeed.y;
                }
                
                // TODO: Player movement logic.......................(0.2p)
                if (enemytimeStop !=1 )
                {
                    if (IsKeyDown (KEY_W) && player.y > 30)
                    {
                        player.y -= playerSpeedY;
                        if (playerTrigger == 1)
                        {
                            switch (playerPower)
                            {
                                case 1: player.y -= playerSpeedY;
                            }
                        }
                    }
                    
                    if (IsKeyDown (KEY_S) && player.y < screenHeight - player.height)
                    {
                        player.y += playerSpeedY;
                        if (playerTrigger == 1)
                        {
                            switch (playerPower)
                            {
                                case 1: player.y += playerSpeedY;
                            }
                        }
                    }
                }
                
                // TODO: Enemy movement logic (IA)...................(1p)
                if (playertimeStop != 1)
                {
                    if (ballPosition.x > ballDetector && twoplayerMode == 0 && difficulty != 0)
                    {
                        if (ballPosition.y > enemy.y + enemy.height/2 && enemy.y < screenHeight - enemy.height)
                        {
                            enemy.y += enemySpeedY;
                        }
                        if (ballPosition.y < enemy.y + enemy.height/2 && enemy.y > 30)
                        {
                            enemy.y -= enemySpeedY; 
                        }
                    }
                    else if (twoplayerMode == 1)
                    {
                        if (IsKeyDown (KEY_UP) && enemy.y > 30)
                        {
                            enemy.y -= enemySpeedY;
                            if (enemyTrigger == 1)
                            {
                                switch (enemyPower)
                                {
                                    case 1: enemy.y -= enemySpeedY;
                                }
                            }
                        }
                    
                        if (IsKeyDown (KEY_DOWN) && enemy.y < screenHeight - enemy.height)
                        {
                            enemy.y += enemySpeedY;
                            if (enemyTrigger == 1)
                            {
                                switch (enemyPower)
                                {
                                    case 1: enemy.y += enemySpeedY;
                                }
                            }
                        }
                    }
                }
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                
                if (CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0)
                {
                    ballSpeed.x *= -1;
                    if (ballSpeed.x < 50)
                    {
                        ballSpeed.x *= 1.2;
                        ballSpeed.y *= 1.2;
                    }
                    if (playerTrigger == 1)
                    {
                        switch (playerPower)
                        {
                            
                            case 2: ballSpeed.x *= 2;
                            break;
                            
                        }
                    }
                }
                
                if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0)
                {
                    ballSpeed.x *= -1;
                    if (ballSpeed.x < 50)
                    {
                        ballSpeed.x *= 1.2;
                        ballSpeed.y *= 1.2;
                    }
                    if (enemyTrigger == 1)
                    {
                        switch (enemyPower)
                        {
                            
                            case 2: ballSpeed.x *= 2;
                            break;
                            
                        }
                    }
                }
                
                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                
                
                if (ballPosition.y <= 30 + ballRadius)
                {
                    ballSpeed.y *= -1;
                }
                if (ballPosition.y >= screenHeight - ballRadius)
                {
                    ballSpeed.y *= -1;
                }
                
                if (ballPosition.x >= screenWidth - ballRadius)
                {
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    ballSpeed.x = - 3;
                    ballSpeed.y = 3;
                    enemylifeFill.width -= enemyLifePoint;
                    enemylifeFill.x += enemyLifePoint;
                    if (enemylifeFill.width <= 0)
                    {
                        enemylifeFill.width = 0;
                        screen = ENDING;
                    }
                    
                }
                if (ballPosition.x <= ballRadius)
                {
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    if (ballSpeed.x < 0)
                    {
                        ballSpeed.x = 3;
                        ballSpeed.y = -3;
                    }
                    
                    playerlifeFill.width -= playerLifePoint;
                    if (playerlifeFill.width <= 0)
                    {
                        playerlifeFill.width = 0;
                        screen = ENDING;
                    }
                }
                
                //TOOLS
                if (cheatMenu == 1)
                {
                    if (IsKeyDown(KEY_U))
                    {
                        playerlifeFill.width -= 3;
                    }
                    if (IsKeyDown(KEY_J))
                    {
                        enemylifeFill.width -= 3;
                        enemylifeFill.x += 3;
                    }
                    if (IsKeyDown(KEY_T))
                    {
                        secondsCounter --;
                    }
                }
                
                // Power Up Logic
                
                if (playerBar == 10 && IsKeyPressed (KEY_D))
                {
                    playerBar = 0;
                    playerTrigger = 1;
                    if (playerPower == 3)
                    {
                        playertimeStop = 1;
                    }
                }
                
                if (enemyBar == 10 && IsKeyPressed (KEY_LEFT))
                {
                    enemyBar = 0;
                    enemyTrigger = 1;
                    if (enemyPower == 3)
                    {
                        enemytimeStop = 1;
                    }
                }
                
                // TODO: Life bars decrease logic....................(1p)

                // TODO: Time counter logic..........................(0.2p)
                
                framesCounter++;
                
                if (framesCounter % 60 == 0)
                {
                    if (playerBar != 10 && playerTrigger == 0 && playerPower != 0)
                    {
                        playerBar++;
                    }
                    if (enemyBar != 10 && enemyTrigger == 0 && enemyPower != 0)
                    {
                        enemyBar++;
                    }
                    if (playerTrigger == 1)
                    {
                        playerPowerTimer--;
                        if (playerPowerTimer == 0)
                        {
                            playerTrigger = 0;
                            playerPowerTimer = 4;
                            playerBar = 0;
                            playertimeStop = 0;
                        }
                    }
                    if (enemyTrigger == 1)
                    {
                        enemyPowerTimer--;
                        if (enemyPowerTimer == 0)
                        {
                            enemyTrigger = 0;
                            enemyPowerTimer = 4;
                            enemyBar = 0;
                            enemytimeStop = 0;
                        }
                    }
                    framesCounter = 0;
                    secondsCounter--;
                }
                if (secondsCounter == 0)
                {
                    screen = ENDING;
                }

                // TODO: Game ending logic...........................(0.2p)
                
                // TODO: Pause button logic..........................(0.2p)
                }
                else 
                {
                    if (IsKeyPressed(KEY_Z))
                    {
                        ballPosition.x = screenWidth/2;
                        ballPosition.y = screenHeight/2;
                        player.y = screenHeight/2;
                        enemy.y = screenHeight/2;
                        playerlifeFill.width = screenWidth/2 - 50;
                        enemylifeFill.width =  screenWidth/2 - 55;
                        enemylifeFill.x = screenWidth/2 + 45;
                        secondsCounter = 99;
                        screen = TITLE;
                        pause = false;
                        playerBar = 0;
                        enemyBar = 0;
                        playerTrigger = 0;
                        enemyTrigger = 0;
                        playertimeStop = 0;
                        enemytimeStop = 0;
                    }
                }
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                framesCounter++;
                
                if (framesCounter % 40 == 0) 
                {
                    framesCounter = 0;
                    blink = !blink;
                }
                
                if (IsKeyPressed(KEY_R))
                {                    
                    screen = GAMEPLAY;
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    player.y = screenHeight/2;
                    enemy.y = screenHeight/2;
                    playerlifeFill.width = screenWidth/2 - 50;
                    enemylifeFill.width =  screenWidth/2 - 55;
                    enemylifeFill.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    playerBar = 0;
                    enemyBar = 0;
                    playerTrigger = 0;
                    enemyTrigger = 0;
                }
                if (IsKeyPressed(KEY_Z))
                {
                    screen = TITLE;
                    ballPosition.x = screenWidth/2;
                    ballPosition.y = screenHeight/2;
                    player.y = screenHeight/2;
                    enemy.y = screenHeight/2;
                    playerlifeFill.width = screenWidth/2 - 50;
                    enemylifeFill.width =  screenWidth/2 - 55;
                    enemylifeFill.x = screenWidth/2 + 45;
                    secondsCounter = 99;
                    playerBar = 0;
                    enemyBar = 0;
                    playerTrigger = 0;
                    enemyTrigger = 0;
                }
                
                
                // TODO: Replay / Exit game logic....................(0.5p)
                
            } break;
            case OPTIONS:
            {
                if (IsKeyPressed(KEY_Z))
                {
                    PlaySound (uisound);
                    screen = TITLE;
                }
                if (IsKeyPressed(KEY_Q))
                {
                    PlaySound (uisound);
                    if (showFrames == 0)
                    {
                        showFrames = 1;
                    }
                    else 
                    {
                        showFrames = 0;
                    }
                }
                if (IsKeyPressed(KEY_W))
                {
                    PlaySound (uisound);
                    if (cheatMenu == 0)
                    {
                        cheatMenu = 1;
                    }
                    else 
                    {
                        cheatMenu = 0;
                    }
                }
                if (IsKeyPressed(KEY_E))
                {
                    PlaySound (uisound);
                    if (difficulty != 4)
                    {
                        difficulty++;
                    }
                    else 
                    {
                        difficulty = 0;
                    }
                }
                if (IsKeyPressed(KEY_R))
                {
                    PlaySound (uisound);
                   if (twoplayerMode == 0)
                    {
                        twoplayerMode = 1;
                    }
                    else 
                    {
                        twoplayerMode = 0;
                    } 
                }
            } break;
            case PLAYERSELECTION:
            {
                if (IsKeyPressed(KEY_A) && playerPower != 0)
                {
                    PlaySound (uisound);
                    playerPower--;
                }
                if (IsKeyPressed(KEY_D) && playerPower != 3)
                {
                    PlaySound (uisound);
                    playerPower++;
                }
                if (IsKeyPressed(KEY_LEFT) && enemyPower != 0)
                {
                    PlaySound (uisound);
                    enemyPower--;
                }
                if (IsKeyPressed(KEY_RIGHT) && enemyPower != 3)
                {
                    PlaySound (uisound);
                    enemyPower++;
                }
                if (IsKeyPressed(KEY_ENTER))
                {
                    PlaySound (uisound);
                    screen = GAMEPLAY;
                }
                if (IsKeyPressed(KEY_Z))
                {
                    PlaySound (uisound);
                    screen = TITLE;
                }
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                     DrawText("Made by Javier Amador", screenWidth/2 - MeasureText("Made by Javier Amador", 40)/2, screenHeight/2 - 50, 40, Fade(BLACK, alphalogo));
                    
                    // TODO: Draw Logo...............................(0.2p)
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    
                    DrawText("Final Pong Game", screenWidth/2 - MeasureText("Final Pong Game", 50)/2, screenHeight/2, 50, Fade(BLACK, alphatitle));
                    
                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    
                    if (blink)
                    {
                        DrawText("Press Enter", screenWidth/2 - MeasureText("Press Enter", 20)/2, screenHeight/2 + 65, 20, BLACK);
                    }
                    DrawText("O. OPTIONS", screenWidth - 100 - MeasureText("O. OPTIONS", 15)/2, screenHeight - 100, 15, Fade(BLACK, alphatitle));
                    
                    
        
                    
                    
                } break; 
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                    DrawCircleV (ballPosition, ballRadius, BLACK);
                    DrawRectangle (0, 0, screenWidth, 30, LIGHTGRAY);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    
                    if (playerTrigger == 1)
                    {
                        DrawRectangleRec (player, BLUE);
                    }
                    else 
                    {
                        DrawRectangleRec (player, BLACK);
                    }
                    
                    if (enemyTrigger == 1)
                    {
                        DrawRectangleRec (enemy, BLUE);
                    }
                    else 
                    {
                        DrawRectangleRec (enemy, BLACK);
                    }
                    
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    
                    DrawRectangleRec (playerlifeBack, GRAY);
                    DrawRectangleRec (enemylifeBack, GRAY);
                    DrawRectangleRec (playerlifeFill, LIME);
                    DrawRectangleRec (enemylifeFill, LIME);
                    
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText ("%d", secondsCounter), screenWidth/2 - MeasureText(FormatText ("d", secondsCounter), 10)/2, 15, 10, BLACK);
                    
                    if (playerPower != 0 && playerBar != 10 && playerTrigger == 0)
                    {
                        DrawText(FormatText("%d" ,playerBar), 100 - MeasureText(FormatText("%d" ,playerBar), 15)/2, screenHeight - 50, 15, Fade(BLUE, alphatitle));
                    }
                    if (enemyPower != 0 && enemyBar != 10 && enemyTrigger == 0)
                    {
                        DrawText(FormatText("%d" ,enemyBar),screenWidth - 100 - MeasureText(FormatText("%d" ,playerBar), 15)/2, screenHeight - 50, 15, Fade(BLUE, alphatitle));
                    }
                    if (playerBar == 10 && playerPower != 0 && playerTrigger == 0)
                    {
                       DrawText("READY (D)", 100 - MeasureText("READY (D)", 15)/2, screenHeight - 50, 15, Fade(BLUE, alphatitle)); 
                    }
                    if (enemyBar == 10 && enemyPower != 0 && enemyTrigger == 0)
                    {
                        DrawText("READY (LEFT)", screenWidth - 100 - MeasureText(FormatText("READY (LEFT)" ,enemyBar), 15)/2, screenHeight - 50, 15, Fade(BLUE, alphatitle)); 
                    }
                    if (playerTrigger == 1)
                    {
                       DrawText("ACTIVE!", 100 - MeasureText("ACTIVE!", 15)/2, screenHeight - 50, 15, Fade(BLUE, alphatitle)); 
                    }
                    if (enemyTrigger == 1)
                    {
                        DrawText("ACTIVE!", screenWidth - 100 - MeasureText(FormatText("ACTIVE!" ,enemyBar), 15)/2, screenHeight - 50, 15, Fade(BLUE, alphatitle)); 
                    }                   
                    
                    
                    
                    // TODO: Draw pause message when required........(0.5p)
                if (pause)
                {
                    DrawRectangle (0, 0, screenWidth, screenHeight, (Color) { 0, 0, 0, 255/2 });
                    DrawText("PAUSE", screenWidth/2 - MeasureText("PAUSE", 40)/2, screenHeight/2, 40, BLACK);
                    DrawText("PRESS P TO RESUME", screenWidth/2 - MeasureText("PRESS P TO RESUME", 20)/2, screenHeight/2 + 50, 20, BLACK);
                    DrawText("Z. TITLE", screenWidth - 100 - MeasureText("Z. TITLE", 15)/2, screenHeight - 100, 15, Fade(BLACK, alphatitle));
                } 
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    if (playerlifeFill.width == 0 || (secondsCounter == 0 && enemylifeFill.width > playerlifeFill.width))
                    {
                        DrawText("YOU LOSE", screenWidth/2 - MeasureText("YOU LOSE", 70)/2, screenHeight/2 - 50, 70, BLACK);
                    }
                    if (enemylifeFill.width == 0 || (secondsCounter == 0 && playerlifeFill.width > enemylifeFill.width))
                    {
                        DrawText("YOU WIN", screenWidth/2 - MeasureText("YOU WIN", 70)/2, screenHeight/2 - 50, 70, BLACK);
                    }
                    if (secondsCounter == 0 && enemylifeFill.width == playerlifeFill.width)
                    {
                        DrawText("DRAW GAME", screenWidth/2 - MeasureText("DRAW GAME", 70)/2, screenHeight/2 - 50, 70, BLACK);
                    }
                    
                    if (blink)
                    {
                        DrawText("PRESS R TO RESTART", screenWidth/2 - MeasureText("PRESS R TO RESTART", 30)/2, screenHeight/2 + 50, 30, BLACK);
                    // TODO: Draw ending message (win or loose)......(0.2p)
                    }
                    DrawText("Z. RETURN", screenWidth - 100 - MeasureText("Z. RETURN", 15)/2, screenHeight - 100, 15, Fade(BLACK, alphatitle));
                    
                } break;
                case OPTIONS:
                {
                    DrawText("OPTIONS", screenWidth/2 - MeasureText("OPTIONS", 50)/2, 150, 50, Fade(BLACK, alphatitle));
                    DrawText(FormatText("Q. SHOW FRAMES: %d", showFrames), screenWidth/2 - MeasureText("Q. SHOW FRAMES", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                    DrawText(FormatText("W. GOD MODE: %d", cheatMenu), screenWidth/2 - MeasureText("W. GOD MODE", 15)/2, 250, 15, Fade(BLACK, alphatitle));
                    if (twoplayerMode == 0)
                    {
                        DrawText(FormatText("E. DIFFICULTY: %d", difficulty), screenWidth/2 - MeasureText("E. DIFFICULTY", 15)/2, 270, 15, Fade(BLACK, alphatitle));
                    }
                    else 
                    {
                        DrawText(FormatText("E. DIFFICULTY: %d", difficulty), screenWidth/2 - MeasureText("E. DIFFICULTY", 15)/2, 270, 15, Fade(GRAY, alphatitle));
                    }
                    DrawText(FormatText("R. TWO PLAYER MODE: %d", twoplayerMode), screenWidth/2 - MeasureText("R. TWO PLAYER MODE", 15)/2, 290, 15, Fade(BLACK, alphatitle));
                    DrawText("Z. RETURN", screenWidth - 100 - MeasureText("Z. RETURN", 15)/2, screenHeight - 100, 15, Fade(BLACK, alphatitle));
                } break;
                case PLAYERSELECTION:
                {
                    DrawText("SELECT YOUR POWER UP", screenWidth/2 - MeasureText("SELECT YOUR POWER UP", 40)/2, 55, 40, BLACK);
                    if (twoplayerMode == 0)
                    {
                        switch (playerPower)
                        {
                            case 0: DrawText("   NONE  D", screenWidth/2 - MeasureText("   NONE  D", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 1: DrawText("A  DOUBLE SPEED  D", screenWidth/2 - MeasureText("A  DOUBLE SPEED  D", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 2: DrawText("A  DOUBLE STRENGHT  D", screenWidth/2 - MeasureText("A  DOUBLE STRENGHT  D", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 3: DrawText("A  STOP TIME   ", screenWidth/2 - MeasureText("A  STOP TIME   ", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break; 
                        }
                    }
                    
                    else 
                    {
                        switch (playerPower)
                        {
                            case 0: DrawText("   NONE  D", screenWidth/4 - MeasureText("   NONE  D", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 1: DrawText("A  DOUBLE SPEED  D", screenWidth/4 - MeasureText("A  DOUBLE SPEED  D", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 2: DrawText("A  DOUBLE STRENGHT  D", screenWidth/4 - MeasureText("A  DOUBLE STRENGHT  D", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 3: DrawText("A  STOP TIME   ", screenWidth/4 - MeasureText("A  STOP TIME   ", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break; 
                        }
                        
                        switch (enemyPower)
                        {
                            case 0: DrawText("   NONE  RIGHT", screenWidth/2 + screenWidth/4 - MeasureText("   NONE  RIGHT", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 1: DrawText("LEFT  DOUBLE SPEED  RIGHT", screenWidth/4 + screenWidth/2 - MeasureText("LEFT  DOUBLE SPEED  RIGHT", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 2: DrawText("LEFT  DOUBLE STRENGHT  RIGHT", screenWidth/4 + screenWidth/2 - MeasureText("LEFT  DOUBLE STRENGHT  RIGHT", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break;
                            case 3: DrawText("LEFT  STOP TIME   ", screenWidth/4 + screenWidth/2 - MeasureText("LEFT  STOP TIME   ", 15)/2, 230, 15, Fade(BLACK, alphatitle));
                            break; 
                        }
                    }
                    DrawText("Z. RETURN", 100 - MeasureText("Z. RETURN", 15)/2, screenHeight - 100, 15, Fade(BLACK, alphatitle));
                    DrawText("ENTER TO PLAY", screenWidth - 100 - MeasureText("ENTER TO PLAY", 15)/2, screenHeight - 100, 15, Fade(BLACK, alphatitle));
                } break;
                default: break;
                
            }
        
        if (showFrames == 1)
        {
            DrawFPS(30, 30);
        }
            
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadSound (battleMusic);
    UnloadSound (mainTheme);
    UnloadSound (uisound);
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}